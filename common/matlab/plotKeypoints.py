import os, io
import matlab.engine

out = None
err = None
eng = None

path = os.path.dirname(os.path.realpath(__file__)) # matlab file in same folder  

class PlotKeypoints:
    def __init__(self, keypoints, shear=[], sensitivity=[], frame_rate=[], position=[], style=''):

        global out, err, eng
        
        if eng is None:
            eng = matlab.engine.start_matlab()
            eng.addpath(eng.genpath(path))      
        if out is None:
            out = io.StringIO()
        if err is None:
            err = io.StringIO()  
            
        keypoints = matlab.double(keypoints.tolist()) 
        shear = matlab.double(shear) 
        sensitivity = matlab.double([sensitivity])
        frame_rate = matlab.double([frame_rate])
        position = matlab.double(position)
        self._style = style
        
        self._plot = eng.PlotKeypoints(keypoints, shear, sensitivity, frame_rate, position, style)           
        
    def update(self, keypoints):
        keypoints = matlab.double(keypoints.tolist())   
        
        if self._style=='' or self._style=='Color': 
            future = eng.update(self._plot, keypoints, nargout=0, stdout=out, stderr=err, background=True)        
        elif self._style=='Time':
            future = eng.updateTime(self._plot, keypoints, nargout=0, stdout=out, stderr=err, background=True)        
        elif self._style=='Velocity':
            future = eng.updateVelocity(self._plot, keypoints, nargout=0, stdout=out, stderr=err, background=True)        
        else:
            future = eng.updateSurface(self._plot, keypoints, nargout=0, stdout=out, stderr=err, background=True)        
        future.result()
        print(out.getvalue(), err.getvalue())
        
    def finish(self, filename=[], flag=[]):
        future = eng.finish(self._plot, filename, flag, nargout=0, stdout=out, stderr=err, background=True)        
        future.result()
        print(out.getvalue(), err.getvalue())
        
def main():
    pass

if __name__ == '__main__':
    main()
