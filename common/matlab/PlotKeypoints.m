classdef PlotKeypoints < handle
    properties  
        hfig
        hvid
        keypoints
        shear = [1 0];
        sensitivity = 0.05;
        frameRate = 10;
        position = [480 0 480 480];
        lim_time = [-10 10];
        lim_velocity = [-2.5 2.5];
        style = ''; % Surface, Contour        
    end  

    methods
        function self = PlotKeypoints(keypoints, shear, sensitivity, frameRate, position, style)   
            self.keypoints = squeeze(keypoints); 
            if ~isempty(shear); self.shear = shear; end
            if ~isempty(sensitivity); self.sensitivity = sensitivity; end
            if ~isempty(frameRate); self.frameRate = frameRate; end
            if ~isempty(position); self.position = position; end
            if length(style); self.style = style; end
                    
            self.hfig = figure('Name',['Keypoints' num2str(round(self.shear),'%i') self.style],... 
                    'Position',self.position, 'color','w', 'MenuBar','None'); 
            
            self.hvid = VideoWriter(fullfile(getenv('TEMPPATH'), [self.hfig.Name '.mp4']), 'MPEG-4');
            self.hvid.FrameRate = self.frameRate;
            open(self.hvid);
        end

        function update(self, keypoints)        
            keypoints = keypoints - median(median(self.keypoints));
            initKeypoints = self.keypoints - median(median(self.keypoints));
                          
            for t = 1 : size(keypoints, 1)
                keypoints_t = squeeze(keypoints(t,:,1:2));   
                shear_t = [keypoints_t(:,1)-initKeypoints(:,1) keypoints_t(:,2)-initKeypoints(:,2)];
                dKeypoints_t = shear_t * self.shear' * self.sensitivity;
                if all(self.shear==0); dKeypoints_t = -rms(shear_t,2) * self.sensitivity; end    
            
                figure(self.hfig)
                plot(155*cosd(0:360), 155*sind(0:360), 'k', 'linewidth',1); hold on           
                if strcmp(self.style,'Color')
                    color = colormap(jet(size(keypoints,2)));
                    [~, order] = sort(initKeypoints(:,2)); [~, order] = sort(order);
                    scatter(-keypoints(t,:,1), -keypoints(t,:,2), [], color(order,:), 'filled', 'MarkerEdgeColor','k')
                else
                    scatter(-keypoints(t,:,1), -keypoints(t,:,2), [], -dKeypoints_t, 'filled', 'MarkerEdgeColor','k')
                    colormap(self.redblue); caxis([-0.5 0.5]); 
                end
                hold off; axis off equal; drawnow
                
                writeVideo(self.hvid, getframe(self.hfig));
            end
        end
                    
        function updateSurface(self, keypoints)
            if ~length(self.style); self.style = 'Surface'; end
            keypoints = keypoints - median(median(self.keypoints));
            initKeypoints = self.keypoints - median(median(self.keypoints));
           
            for t = 1 : size(keypoints, 1)
                keypoints_t = squeeze(keypoints(t,:,1:2));              
                shear_t = [keypoints_t(:,1)-initKeypoints(:,1) keypoints_t(:,2)-initKeypoints(:,2)];
                dKeypoints_t = shear_t * self.shear' * self.sensitivity;
                if all(self.shear==0); dKeypoints_t = -rms(shear_t,2) * self.sensitivity; end    

                S = fit(keypoints_t, -dKeypoints_t, 'cubicinterp');
                
                figure(self.hfig)
                p = plot(S, 'style',self.style); set(gca, 'xdir','reverse'); 
                if self.style=='Contour'; p.LevelStep = 0.05; set(gca,'ydir','reverse'); end
                if self.style=='Surface'; view(0,-45); end 
                colormap(self.redblue); caxis([-0.5 0.5]);
                zlim([-0.5 0.5]); axis off

                writeVideo(self.hvid, getframe(self.hfig));
            end
        end

        function updateTime(self, keypoints)
            keypoints = keypoints - median(median(self.keypoints));
            initKeypoints = self.keypoints - median(median(self.keypoints));           
           
            for t = 1 : size(keypoints, 1)
                keypoints_t = squeeze(keypoints(t,:,1:2));              
                shear_t = [keypoints_t(:,1)-initKeypoints(:,1) keypoints_t(:,2)-initKeypoints(:,2)];
                dKeypoints(t,:) = shear_t * self.shear';
                if all(self.shear==0); dKeypoints(t,:) = rms(shear_t,2); end    
            end
                       
            color = colormap(jet(size(keypoints,2)));
            [~, order] = sort(initKeypoints(:,2)); [~, order] = sort(order);
 
            figure(self.hfig) 
            for k = 1 : size(keypoints, 2)
                plot(dKeypoints(:,k), 'color', color(order(k),:)); hold on
            end
            axis([0 size(keypoints,1) self.lim_time]); grid on
            xlabel('time (frames)'); ylabel('displacement (pixels)'); set(gca, 'fontsize',12);  
            
            writeVideo(self.hvid, getframe(self.hfig));
        end        

        function updateVelocity(self, keypoints)
            keypoints = keypoints - median(median(self.keypoints));
            initKeypoints = self.keypoints - median(median(self.keypoints));           
           
            for t = 1 : size(keypoints, 1)
                keypoints_t = squeeze(keypoints(t,:,1:2));              
                shear_t = [keypoints_t(:,1)-initKeypoints(:,1) keypoints_t(:,2)-initKeypoints(:,2)];
                dKeypoints(t,:) = shear_t * self.shear';
                if all(self.shear==0); dKeypoints(t,:) = rms(shear_t,2); end    
            end
                       
            color = colormap(jet(size(keypoints,2)));
            [~, order] = sort(initKeypoints(:,2)); [~, order] = sort(order);
 
            figure(self.hfig) 
            for k = 1 : size(keypoints, 2)
                plot(diff(dKeypoints(:,k)), 'color', color(order(k),:)); hold on
            end
            axis([0 size(keypoints,1) self.lim_velocity]); grid on
            xlabel('time (frames)'); ylabel('velocity (pixels/frame)'); set(gca, 'fontsize',12);  
            
            writeVideo(self.hvid, getframe(self.hfig));
        end              

        function finish(self, filename, flag)
            if isempty(filename); filename = self.hfig.Name; end
            if isempty(flag); flag = filename(end-3:end); filename(end-3:end) = []; end
            close(self.hvid);
            vidFile = fullfile(self.hvid.Path,self.hvid.Filename);
            set(self.hfig, 'units', 'inches', 'paperunits', 'inches')
            if contains(flag,'fig'); savefig(self.hfig, [filename '.fig']); end
            if contains(flag,'mp4'); try movefile(vidFile, [filename '.mp4']); end; end
            if contains(flag,'png'); set(self.hfig, 'position', [0 0 3.5 3.5], 'papersize', [3.5 3.5]); end
            if contains(flag,'png'); print(self.hfig, [filename '.png'], '-r300', '-dpng'); end        
        end  
        
        function c = redblue(self)
            n = 50;
            r = (0:n-1)'/max(n,1); g = r;
            c = [[r; ones(n+1,1)] [g; 1; flipud(g)] [ones(n+1,1); flipud(r)]];
        end  
    end 
end

