classdef TransformVoronoi < handle
    properties
        medianX
        
        maxEdge       % threshold for removing edges
        borderSpace   % space between border points: 20 hex; 10 Shadow
        borderScale   % scales the border size
    end
    
    methods
        function self = TransformVoronoi(varargin)
            p = inputParser;
            p.addOptional('maxEdge', 100);
            p.addOptional('borderSpace', 20);
            p.addOptional('borderScale', 1.2);
            p.parse(varargin{:});
            p = p.Results;
            
            self.maxEdge = p.maxEdge;
            self.borderSpace = p.borderSpace;
            self.borderScale = p.borderScale;
        end
        
        function [A,Cx,Cy] = process(self, X)
            for t = 1 : size(X,1)
                [A(t,:), Cx(t,:), Cy(t,:)] = self.transform(X(t,:,:));
            end
            Cx = Cx(:); 
            Cy = Cy(:);
        end
        
        function [A,Cx,Cy] = transform(self, X)
            % Voronoi tesselation function
            % Input: X = (x,y) pin positions (centroids)
            % Returns: Y = (x,y,A) pin positions and cell areas
            %          C cells are lists of vertices
            X = squeeze(X);
            if isempty(self.medianX); self.medianX = median(X); end
            X = X - self.medianX + 0.1*rand(size(X));
            
            % apply voronoi to data + boundary: V vertices, C cells
            BX = self.extendBoundary(X);
            [BV, BC] = voronoin([BX(:,1), BX(:,2)], {'Qbb'});
            
            % prune edges outside boundary
            maxE = cellfun(@(x) max(self.vert2edge(BV,x)), BC);
            Cx = cellfun(@(x) BV(x,1), BC(maxE<self.maxEdge), 'un',0);
            Cy = cellfun(@(x) BV(x,2), BC(maxE<self.maxEdge), 'un',0);
            A = cellfun(@(x,y) polyarea(x,y), Cx, Cy);
        end
        
        function Y = extendBoundary(self, X)
            
            % Generate boundary on the centroids.
            B = boundary(X(:,1),X(:,2),0);
            BX = [X(B,1)*self.borderScale, X(B,2)*self.borderScale];
            
            % identify points on boundary
            diffBX = diff(BX(:,1)).^2 + diff(BX(:,2)).^2;
            n = floor(sqrt(diffBX)/self.borderSpace);
            
            % generate new points on including boundary
            BX1 = arrayfun(@linspace, BX(1:end-1,1),BX(2:end,1),n, 'un',0);
            BX2 = arrayfun(@linspace, BX(1:end-1,2),BX(2:end,2),n, 'un',0);
            Y = unique([X; [BX1{:}; BX2{:}]'], 'rows', 'stable');
        end
        
        function E = vert2edge(self, V,C)
            r1 = 1:length(C); r2 = [2:length(C),1];
            E = rms([V(C(r1),1)-V(C(r2),1), V(C(r1),2)-V(C(r2),2)],2);
        end
    end
end