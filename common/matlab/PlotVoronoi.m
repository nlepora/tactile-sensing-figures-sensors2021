classdef PlotVoronoi < handle
    properties
        hfig
        hvid
        areas
        sensitivity = 0.05;
        frameRate = 10;
        position = [0, 0, 480, 480];
        lim_time = [-50 250];
        lim_velocity = [-45 45];
        style = ''; % Surface, Contour
    end
    
    methods
        function self = PlotVoronoi(areas, sensitivity, frameRate, position, style)           
            self.areas = areas;
            if ~isempty(sensitivity); self.sensitivity = sensitivity; end
            if ~isempty(frameRate); self.frameRate = frameRate; end
            if ~isempty(position); self.position = position; end
            if length(style); self.style = style; end
            
            self.hfig = figure('Name',['Voronoi' self.style], ...
                    'Position',self.position, 'color','w', 'MenuBar','None'); 

            self.hvid = VideoWriter(fullfile(getenv('TEMPPATH'), [self.hfig.Name '.mp4']), 'MPEG-4');
            self.hvid.FrameRate = self.frameRate;
            open(self.hvid);
        end
       
        function update(self, areas, cx, cy)
            [nt, ncells] = size(areas);
            cx = reshape(cx, [nt, ncells]);
            cy = reshape(cy, [nt, ncells]);          
                                            
            for t = 1 : nt
                dArea_t = areas(t,:) - self.areas;
                cx_t = cell2mat(cellfun(@(x) self.scale(x,ncells), cx(t,:), 'un',0));
                cy_t = cell2mat(cellfun(@(x) self.scale(x,ncells), cy(t,:), 'un',0));
                
                figure(self.hfig); clf
                plot(155*cosd(0:360), 155*sind(0:360), 'k', 'linewidth',1); hold on           
                patch(-cx_t, -cy_t, dArea_t * self.sensitivity^2, 'EdgeColor','k');
                colormap(self.redblue); caxis([-0.5 0.5]); 
                pbaspect([1 1 1]); axis off equal; 
               
                writeVideo(self.hvid, getframe(self.hfig));
            end
        end
        
        function updateSurface(self, keypoints, areas)
            if ~length(self.style); self.style = 'Surface'; end
 
            for t = 1 : size(areas, 1)
                dArea_t = areas(t,:) - self.areas;
                keypoints_t = squeeze(keypoints(t,:,1:2));
                S = fit(keypoints_t, dArea_t' * self.sensitivity^2, 'cubicinterp');
                
                figure(self.hfig)
                plot(S, 'style',self.style); set(gca, 'xdir','reverse'); 
                if self.style=='Contour'; p.LevelStep = 0.05; set(gca,'ydir','reverse'); end
                if self.style=='Surface'; view(0,-45); end 
                colormap(self.redblue); caxis([-0.5 0.5]);
                zlim([-1 1]); axis off

                writeVideo(self.hvid, getframe(self.hfig));
            end
        end

        function updateTime(self, keypoints, areas)
                                           
            for t = 1 : size(areas, 1)
                dArea(t,:) = areas(t,:) - self.areas;
            end
                       
            color = colormap(jet(size(areas,2)));
            [~, order] = sort(keypoints(1,:,2)); [~, order] = sort(order);
                        
            figure(self.hfig)
            for k = 1 : size(areas, 2)
                plot(dArea(:,k), 'color', color(order(k),:)); hold on
            end
            axis([0 size(areas,1) self.lim_time]); set(gca, 'fontsize',12); grid on
            xlabel('time (frames)'); ylabel('area (pixels^2)') 
               
            writeVideo(self.hvid, getframe(self.hfig));
        end

        function updateVelocity(self, keypoints, areas)
                                           
            for t = 1 : size(areas, 1)
                dArea(t,:) = areas(t,:) - self.areas;
            end
                       
            color = colormap(jet(size(areas,2)));
            [~, order] = sort(keypoints(1,:,2)); [~, order] = sort(order);
                        
            figure(self.hfig)
            for k = 1 : size(areas, 2)
                plot(diff(dArea(:,k)), 'color', color(order(k),:)); hold on
            end
            axis([0 size(areas,1) self.lim_velocity]); set(gca, 'fontsize',12); grid on
            xlabel('time (frames)'); ylabel('area change (pixels^2/frame)') 
               
            writeVideo(self.hvid, getframe(self.hfig));
        end

        function finish(self, filename, flag)
            if isempty(filename); filename = self.hfig.Name; end
            if isempty(flag); flag = filename(end-3:end); filename(end-3:end) = []; end
            close(self.hvid);
            vidFile = fullfile(self.hvid.Path,self.hvid.Filename);
            set(self.hfig, 'units', 'inches', 'paperunits', 'inches')
            if contains(flag,'fig'); savefig(self.hfig, [filename '.fig']); end
            if contains(flag,'mp4'); try movefile(vidFile, [filename '.mp4']); end; end
            if contains(flag,'png'); set(self.hfig, 'position', [0 0 3.5 3.5], 'papersize', [3.5 3.5]); end
            if contains(flag,'png'); print(self.hfig, [filename '.png'], '-r300', '-dpng'); end        
        end  
        
        function C = scale(self, C, D)
            nC = length(C);
            if nC < D; C = C([1:nC,(ones(1,D-nC)*nC)]); end
        end

        function c = redblue(self)
            n = 50;
            r = (0:n-1)'/max(n,1); g = r;
            c = [[r; ones(n+1,1)] [g; 1; flipud(g)] [ones(n+1,1); flipud(r)]];
        end       
    end
end