import io, os
import numpy as np
import matlab.engine

out = None
err = None
eng = None

path = os.path.dirname(os.path.realpath(__file__)) # matlab file in same folder  

class TransformVoronoi:
    def __init__(self):

        global out, err, eng
        
        if eng is None:
            eng = matlab.engine.start_matlab()
            eng.addpath(eng.genpath(path))   
        if out is None:
            out = io.StringIO()
        if err is None:
            err = io.StringIO()          

        self._model = eng.TransformVoronoi()
        
    def process(self, keypoints):
        try: 
            keypoints = matlab.double(keypoints[:,:,:2].tolist())   
        except: 
            keypoints =  matlab.double(keypoints[np.newaxis,:,:2].tolist())
        
        future = eng.process(self._model, keypoints, nargout=3, stdout=out, stderr=err, background=True)        
        areas, cx, cy = future.result()
        areas = np.asarray(areas)

        print(out.getvalue(), err.getvalue())
        
        return areas, cx, cy       

def main():
    pass
    
if __name__ == '__main__':
    main()
