import os, io
import matlab.engine

out = None
err = None
eng = None

path = os.path.dirname(os.path.realpath(__file__)) # matlab file in same folder  

class PlotFrames:
    def __init__(self, frame_rate=[], position=[]):

        global out, err, eng
        
        if eng is None:
            eng = matlab.engine.start_matlab()
            eng.addpath(eng.genpath(path))      
        if out is None:
            out = io.StringIO()
        if err is None:
            err = io.StringIO()  

        frame_rate = matlab.double([frame_rate])            
        position = matlab.double(position)           
            
        self._plot = eng.PlotFrames(frame_rate, position)           
        
    def update(self, frame, y=[]):
        frame = matlab.double(frame.tolist()) 
        if len(y)>0: y = y.tolist()
        y = matlab.double(y)
        
        future = eng.update(self._plot, frame, y, stdout=out, stderr=err, background=True, nargout=0)        
        future.result()
        print(out.getvalue(), err.getvalue())
        
    def finish(self, filename=[], flag=[]):
        future = eng.finish(self._plot, filename, flag, stdout=out, stderr=err, background=True, nargout=0)        
        future.result()
        print(out.getvalue(), err.getvalue())
        
def main():
    pass

if __name__ == '__main__':
    main()
