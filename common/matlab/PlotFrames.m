classdef PlotFrames < handle
    properties  
        hfig
        hvid
        frameRate = 10;
        position = [0 0 480 480];
    end  

    methods
        function self = PlotFrames(frameRate, position)                         
            if ~isempty(frameRate); self.frameRate = frameRate; end
            if ~isempty(position); self.position = position; end
            
            self.hfig = figure('Name','Frames', 'Position',self.position, 'color','w', 'MenuBar','None'); 
            clf; hold; axis off equal
            
            self.hvid = VideoWriter(fullfile(getenv('TEMPPATH'), [self.hfig.Name '.mp4']), 'MPEG-4');
            self.hvid.FrameRate = self.frameRate;
            open(self.hvid);
        end

        function update(self, frame, y)
            for i = 1:size(frame, 1)
                figure(self.hfig)
                imshow(squeeze(frame(i,:,:,:)),...
                     'InitialMagnification','fit', 'border','tight')     
                if ~isempty(y)
                    text(size(frame,2)/2,size(frame,2)/35, ['pose = (' sprintf(' %4.1f',y) ' )'], 'color','r', ...
                        'fontsize',20, 'fontweight','bold', 'horizontalalignment','center')
                end
                hold off; axis off equal; drawnow
                
                writeVideo(self.hvid, getframe(self.hfig));
            end
        end

        function finish(self, filename, flag)
            if isempty(filename); filename = self.hfig.Name; end
            if isempty(flag); flag = filename(end-3:end); filename(end-3:end) = []; end
            close(self.hvid);
            vidFile = fullfile(self.hvid.Path,self.hvid.Filename);
            set(self.hfig, 'units', 'inches', 'paperunits', 'inches')
            if contains(flag,'fig'); savefig(self.hfig, [filename '.fig']); end
            if contains(flag,'mp4'); try movefile(vidFile, [filename '.mp4']); end; end
            if contains(flag,'png'); set(self.hfig, 'position', [0 0 3.5 3.5], 'papersize', [3.5 3.5]); end
            if contains(flag,'png'); print(self.hfig, [filename '.png'], '-r300', '-dpng'); end        
        end         
        
    end 
end


