# issues:
# 1. self._writer does not fully terminate interactive mode
#   fix by event_source.stop() - but have not found how to access
# 2. wm_geometry() not working to place window
#   in TkAgg needed for interactive mode

import os
import matplotlib
import matplotlib.pylab as plt
from matplotlib import animation

temp_path = os.environ["TEMPPATH"]

class PlotFrames:
    def __init__(self, name="frames", frame_rate=10, position=[0, 0, 480, 480]):
        self._name = name
        self._fig = plt.figure(name, figsize=(3.5, 3.5))
        self._ax = self._fig.add_subplot(111)

        mgr = plt.get_current_fig_manager()
        if matplotlib.get_backend()=="TkAgg": mgr.window.wm_geometry("{}x{}".format(*position[2:]))  
        if matplotlib.get_backend()=="QT5Agg": mgr.window.setGeometry(*position)

        self._writer = animation.writers["ffmpeg"](fps=frame_rate)
        self._writer.setup(self._fig, os.path.join(temp_path,name+".mp4"), dpi=100)

    def update(self, frames, y=[]):
        plt.cla()
        for frame in frames:
            self._ax.imshow(frame, cmap="Greys")
            self._ax.set_xticks([]); self._ax.set_yticks([])

            plt.pause(0.0001)
            self._writer.grab_frame()
        self._fig.show()
        
    def finish(self, path=temp_path):
        self._fig.savefig(os.path.join(path,self._name+".png"), bbox_inches="tight") 
        self._writer.finish() 

def main():
    pass

if __name__ == "__main__":
    main()
