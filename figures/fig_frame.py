import os, sys, numpy, time
from cri.robot import SyncRobot
from cri.controller import Mg400Controller as Controller
from vsp.video_stream import CvImageOutputFileSeq, CvVideoDisplay, CvPreprocVideoCamera 
from vsp.processor import CameraStreamProcessor, AsyncProcessor
from vsp.detector import CvBlobDetector
from vsp.tracker import NearestNeighbourTracker
from vsp.encoder import KeypointEncoder
from vsp.view import KeypointView

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + r'\..')
from common.matlab.transformVoronoi import TransformVoronoi
from common.matlab.plotVoronoi import PlotVoronoi
from common.matlab.plotKeypoints import PlotKeypoints
from common.matlab.plotFrames import PlotFrames


# NB amcap: reset all settings; autoexposure off; saturdation max
def make_sensor(keypoints=None, crop=[320-128-30, 240-128-10, 320+128-30, 240+128-10], exposure=-6):
    camera = CvPreprocVideoCamera(source=1, crop=crop, exposure=exposure) #CvVideoCamera(source=1)
    for _ in range(5): camera.read() # Hack - camera transient
    return AsyncProcessor(CameraStreamProcessor(camera=camera,
            pipeline=[
                CvBlobDetector(
                    min_threshold=33,
                    max_threshold=199,
                    filter_by_color=True,
                    blob_color=255,
                    filter_by_area=True,
                    min_area=13.4,#23.4,
                    max_area=200,#119,
                    filter_by_circularity=True,
                    min_circularity=0.42,
                    filter_by_inertia=True,
                    min_inertia_ratio=0.29,
                    filter_by_convexity=True,
                    min_convexity=0.57,           
                ),
                NearestNeighbourTracker(threshold=20, keypoints=keypoints),
                KeypointEncoder(),
            ],
            view=KeypointView(color=(0,255,0)),
            writer=CvImageOutputFileSeq(),
        ))

def make_camera_bw(size=None, crop=[320-128-10, 240-128+10, 320+128-10, 240+128+10], threshold=(61,-5), exposure=-7, **kwargs):
    camera = CvPreprocVideoCamera(size, crop, threshold, source=1, exposure=exposure)
    for _ in range(5): camera.read() # Hack - camera transient    
    return AsyncProcessor(CameraStreamProcessor(camera=camera, 
            display=CvVideoDisplay(name='sensor'),
            writer=CvImageOutputFileSeq(),
        ))

def make_camera(size=None, crop=[320-128-10, 240-128+10, 320+128-10, 240+128+10], exposure=-7, **kwargs):
    camera = CvPreprocVideoCamera(size, crop, source=1, exposure=exposure)
    for _ in range(5): camera.read() # Hack - camera transient    
    return AsyncProcessor(CameraStreamProcessor(camera=camera, 
            display=CvVideoDisplay(name='sensor'),
            writer=CvImageOutputFileSeq(),
        ))


def main():    
    home_dir = os.path.join(os.environ['DATAPATH'], 'TacTip_dobot', 
                            'figures', 'fig_frame_'+time.strftime('%m%d%H%M'))
    os.makedirs(home_dir, exist_ok=True)
       
    # initialize
    voronoi = TransformVoronoi()
    with make_sensor() as sensor:       
        init_keypoints = sensor.process(num_frames=1, start_frame=1)[0,]
        init_areas, cx, cy = voronoi.process(init_keypoints)
        
        numpy.save(os.path.join(home_dir, 'keypoints_init'), init_keypoints)
        numpy.save(os.path.join(home_dir, 'areas_init'), init_areas)

    fig = [PlotFrames(), 
           PlotFrames(), 
           PlotKeypoints(init_keypoints, shear=[1,0], style=''),
           PlotKeypoints(init_keypoints, shear=[1,0], style='Contour'),
           PlotKeypoints(init_keypoints, shear=[0,1], style=''),
           PlotKeypoints(init_keypoints, shear=[0,1], style='Contour'),
           PlotKeypoints(init_keypoints, shear=[0,0], style=''),
           PlotKeypoints(init_keypoints, shear=[0,0], style='Contour'),
           PlotVoronoi(init_areas, style=''),
           PlotVoronoi(init_areas, style='Contour')]
    
    # capture frames
    pose_home = numpy.array([310, 0, -107, 0, 0, 0]  )
    
    with SyncRobot(Controller()) as robot:
        robot.linear_speed = 10#, robot.angular_speed = (100, 100)
        robot.coord_frame = [0, 0, 0, 0, 0, 0]
        robot.move_linear(pose_home)

        with make_camera() as sensor: 
            robot.move_linear(pose_home)            
            robot.move_linear(pose_home + [0, 0, -11, 0, 0, 0])
            frames = sensor.process(num_frames=1, start_frame=1) 
            robot.move_linear(pose_home)
            
            # output
            fig[0].update(frames/255)    
            fig[0].finish(os.path.join(home_dir, 'fig5a1'), 'png fig')              

        with make_camera_bw() as sensor: 
            robot.move_linear(pose_home)            
            robot.move_linear(pose_home + [0, 0, -11, 0, 0, 0])
            frames = sensor.process(num_frames=1, start_frame=1) 
            robot.move_linear(pose_home)
            
            # output
            fig[1].update(frames/255)    
            fig[1].finish(os.path.join(home_dir, 'fig5a2'), 'png fig')          
            
        with make_sensor(init_keypoints) as sensor: 
            robot.move_linear(pose_home)            
            robot.move_linear(pose_home + [0, 0, -11, 0, 0, 0])
            keypoints = sensor.process(num_frames=1, start_frame=1) 
            areas, cx, cy = voronoi.process(keypoints)
            robot.move_linear(pose_home)
                    
            # output
            fig[2].update(keypoints)
            fig[2].finish(os.path.join(home_dir, 'fig5b1'), 'png fig')              
            fig[3].update(keypoints)
            fig[3].finish(os.path.join(home_dir, 'fig5b2'), 'png fig')   
            
            fig[4].update(keypoints)    
            fig[4].finish(os.path.join(home_dir, 'fig5c1'), 'png fig')
            fig[5].update(keypoints)    
            fig[5].finish(os.path.join(home_dir, 'fig5c2'), 'png fig')

            fig[6].update(keypoints)    
            fig[6].finish(os.path.join(home_dir, 'fig5d1'), 'png fig')
            fig[7].update(keypoints)    
            fig[7].finish(os.path.join(home_dir, 'fig5d2'), 'png fig')
            
            fig[8].update(keypoints, areas, cx, cy)    
            fig[8].finish(os.path.join(home_dir, 'fig5e1'), 'png fig')
            fig[9].update(keypoints, areas, cx, cy)    
            fig[9].finish(os.path.join(home_dir, 'fig5e2'), 'png fig')
            
        robot.move_linear(pose_home)
                     
               
if __name__ == '__main__':
    main()
