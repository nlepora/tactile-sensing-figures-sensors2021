# -*- coding: utf-8 -*-

import os, numpy, time
    
# Local imports
from core.sensor.preproc_video_camera import CvPreprocVideoCamera  
from core.sensor.voronoi.transformVoronoi import TransformVoronoi
from core.utils.plotVoronoi import PlotVoronoi
from core.utils.plotKeypoints import PlotKeypoints

# Third party imports
from cri.robot import AsyncRobot
from cri_dobot.robot import SyncDobot
from cri_dobot.controller import dobotMagicianController
from vsp.video_stream import CvImageOutputFileSeq#, CvVideoDisplay 
from vsp.processor import CameraStreamProcessor, AsyncProcessor
from vsp.detector import CvBlobDetector
from vsp.tracker import NearestNeighbourTracker
from vsp.encoder import KeypointEncoder
from vsp.view import KeypointView


def make_robot():
    return AsyncRobot(SyncDobot(dobotMagicianController()))

# NB amcap: reset all settings; autoexposure off; saturdation max
def make_sensor(keypoints=None, crop=[160-22,70+12,490-22,400+12], exposure=-7):
    camera = CvPreprocVideoCamera(source=0, crop=crop, exposure=exposure) #CvVideoCamera(source=1)
    for _ in range(5): camera.read() # Hack - camera transient
    return AsyncProcessor(CameraStreamProcessor(camera=camera,
            pipeline=[
                CvBlobDetector(
                    min_threshold=33,
                    max_threshold=199,
                    filter_by_color=True,
                    blob_color=255,
                    filter_by_area=True,
                    min_area=23.4,
                    max_area=119,
                    filter_by_circularity=True,
                    min_circularity=0.42,
                    filter_by_inertia=True,
                    min_inertia_ratio=0.29,
                    filter_by_convexity=True,
                    min_convexity=0.57,           
                ),
                NearestNeighbourTracker(threshold=20, keypoints=keypoints),
                KeypointEncoder(),
            ],
            view=KeypointView(color=(0,255,0)),
            writer=CvImageOutputFileSeq(),
        ))


def main():    
    home_dir = os.path.join(os.environ['DATAPATH'], 'TacTip_dobot', 
                            'figures', 'video_edge_'+time.strftime('%m%d%H%M'))
    os.makedirs(home_dir, exist_ok=True)
       
    # initialize
    voronoi = TransformVoronoi()
    with make_sensor() as sensor:       
        init_keypoints = sensor.process(num_frames=1, start_frame=1)[0,]
        init_areas, cx, cy = voronoi.process(init_keypoints)
        
        numpy.save(os.path.join(home_dir, 'keypoints_init'), init_keypoints)
        numpy.save(os.path.join(home_dir, 'areas_init'), init_areas)
    
    # loop test 
    pose_home = numpy.array([197, -1, 0, 0, 0, 0])

    with make_robot() as robot, make_sensor(init_keypoints) as sensor: 
        robot.linear_speed, robot.angular_speed = (100, 100)
        robot.coord_frame = [0, 0, 0, 0, 0, 0]
        robot.move_linear(pose_home)

        fig = [PlotKeypoints(init_keypoints, shear=[1,0], position=[0, 60, 480, 480], style=''),
               PlotKeypoints(init_keypoints, shear=[1,0], position=[480, 60, 480, 480], style='Contour'),
               PlotKeypoints(init_keypoints, shear=[0,1], position=[0, 60, 480, 480], style=''),
               PlotKeypoints(init_keypoints, shear=[0,1], position=[960, 60, 480, 480], style='Contour'),
               PlotVoronoi(init_areas, position=[0, 540, 480, 480], style=''),
               PlotVoronoi(init_areas, position=[480, 540, 480, 480], style='Surface')]
        
        for a, angle in enumerate(range(-90, 180-90-2, 15)):
        
            robot.move_linear(pose_home + [0, 0, -6, 0, 0, -0+angle])            
            robot.move_linear(pose_home + [0, 0, -11, 0, 0, -0+angle])
            keypoints = sensor.process(num_frames=1, start_frame=1) 
            robot.move_linear(pose_home + [0, 0, -6, 0, 0, -0+angle])
            areas, cx, cy = voronoi.process(keypoints)
            
            # output
            for f in fig[:-2]: f.update(keypoints)
            for f in fig[-2:]: f.update(keypoints, areas, cx, cy)

        for i, f in enumerate(fig): 
            f.finish(os.path.join(home_dir, f'video_edge_{i}'), 'mp4 png fig')
                
        robot.move_linear(pose_home)
                     
               
if __name__ == '__main__':
    main()
