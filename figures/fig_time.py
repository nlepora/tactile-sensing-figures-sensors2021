# -*- coding: utf-8 -*-

import os, numpy, time
    
# Local imports
from core.sensor.preproc_video_camera import CvPreprocVideoCamera  
from core.sensor.voronoi.transformVoronoi import TransformVoronoi
from core.utils.plotVoronoi import PlotVoronoi
from core.utils.plotKeypoints import PlotKeypoints

# Third party imports
from cri.robot import AsyncRobot
from cri_dobot.robot import SyncDobot
from cri_dobot.controller import dobotMagicianController
from vsp.video_stream import CvImageOutputFileSeq
from vsp.processor import CameraStreamProcessor, AsyncProcessor
from vsp.detector import CvBlobDetector
from vsp.tracker import NearestNeighbourTracker
from vsp.encoder import KeypointEncoder
from vsp.view import KeypointView


def make_robot():
    return AsyncRobot(SyncDobot(dobotMagicianController()))

# NB amcap: reset all settings; autoexposure off; saturdation max
def make_sensor(keypoints=None, crop=[160-22,70+12,490-22,400+12], exposure=-7):
    camera = CvPreprocVideoCamera(source=0, crop=crop, exposure=exposure) #CvVideoCamera(source=1)
    for _ in range(5): camera.read() # Hack - camera transient
    return AsyncProcessor(CameraStreamProcessor(camera=camera,
            pipeline=[
                CvBlobDetector(
                    min_threshold=33,
                    max_threshold=199,
                    filter_by_color=True,
                    blob_color=255,
                    filter_by_area=True,
                    min_area=23.4,
                    max_area=119,
                    filter_by_circularity=True,
                    min_circularity=0.42,
                    filter_by_inertia=True,
                    min_inertia_ratio=0.29,
                    filter_by_convexity=True,
                    min_convexity=0.57,           
                ),
                NearestNeighbourTracker(threshold=20, keypoints=keypoints),
                KeypointEncoder(),
            ],
            view=KeypointView(color=(0,255,0)),
            writer=CvImageOutputFileSeq(),
        ))


def main():    
    home_dir = os.path.join(os.environ['DATAPATH'], 'TacTip_dobot', 
                            'figures', 'fig_time_'+time.strftime('%m%d%H%M'))
    os.makedirs(home_dir, exist_ok=True)
       
    # initialize
    voronoi = TransformVoronoi()
    with make_sensor() as sensor:       
        init_keypoints = sensor.process(num_frames=1, start_frame=1) 
        init_areas, cx, cy = voronoi.process(init_keypoints)

        numpy.save(os.path.join(home_dir, 'keypoints_init'), init_keypoints)
        numpy.save(os.path.join(home_dir, 'areas_init'), init_areas)

    fig = [PlotKeypoints(init_keypoints, position=[0, 60, 480, 480], style='Color'),
           PlotKeypoints(init_keypoints, shear=[1,0], position=[480, 60, 480, 480], style='Time'),
           PlotKeypoints(init_keypoints, shear=[1,0], position=[480, 60, 480, 480], style='Velocity'),
           PlotKeypoints(init_keypoints, shear=[0,1], position=[960, 60, 480, 480], style='Time'),
           PlotKeypoints(init_keypoints, shear=[0,1], position=[960, 60, 480, 480], style='Velocity'),
           PlotKeypoints(init_keypoints, shear=[0,0], position=[960, 60, 480, 480], style='Time'),
           PlotKeypoints(init_keypoints, shear=[0,0], position=[960, 60, 480, 480], style='Velocity'),
           PlotVoronoi(init_areas, position=[1440, 60, 480, 480], style='Time'),
           PlotVoronoi(init_areas, position=[1440, 60, 480, 480], style='Velocity')]
    
    fig[0].update(init_keypoints)
    fig[0].finish(os.path.join(home_dir, 'fig5z'), 'png fig')

    # capture frames
    pose_home = numpy.array([197, -1, 0, 0, 0, 0])

    with make_robot() as robot, make_sensor(keypoints=init_keypoints[0,]) as sensor: 
        robot.linear_speed, robot.angular_speed = (100, 100)
        robot.coord_frame = [0, 0, 0, 0, 0, 0]
        robot.move_linear(pose_home)
        
        robot.move_linear(pose_home + [0, 0, -6, 0, 0, 0])
        sensor.async_process(num_frames=40, start_frame=1) 
        robot.move_linear(pose_home + [0, 0, -10.75, 0, 0, 0])
        robot.move_linear(pose_home + [0, 0, -6, 0, 0, 0])
        keypoints = sensor.async_result()
        areas, cx, cy = voronoi.process(keypoints)
                
        # output
        fig[1].update(keypoints)
        fig[1].finish(os.path.join(home_dir, 'fig5f1'), 'png fig')         
        fig[2].update(keypoints)
        fig[2].finish(os.path.join(home_dir, 'fig5f2'), 'png fig')   
        
        fig[3].update(keypoints)    
        fig[3].finish(os.path.join(home_dir, 'fig5g1'), 'png fig')
        fig[4].update(keypoints)    
        fig[4].finish(os.path.join(home_dir, 'fig5g2'), 'png fig')
        
        fig[5].update(keypoints)    
        fig[5].finish(os.path.join(home_dir, 'fig5h1'), 'png fig')
        fig[6].update(keypoints)
        fig[6].finish(os.path.join(home_dir, 'fig5h2'), 'png fig')         

        fig[7].update(keypoints, areas, cx, cy)    
        fig[7].finish(os.path.join(home_dir, 'fig5i1'), 'png fig')
        fig[8].update(keypoints, areas, cx, cy)    
        fig[8].finish(os.path.join(home_dir, 'fig5i2'), 'png fig')
        
        robot.move_linear(pose_home)
                     
               
if __name__ == '__main__':
    main()
