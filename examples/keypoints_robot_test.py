from vsp.video_stream import CvImageOutputFileSeq, CvVideoDisplay, CvPreprocVideoCamera  
from vsp.processor import CameraStreamProcessor, AsyncProcessor
from vsp.detector import CvBlobDetector
from vsp.tracker import NearestNeighbourTracker
from vsp.encoder import KeypointEncoder
from vsp.view import KeypointView

from cri.robot import SyncRobot
from cri.controller import Mg400Controller as Controller


def make_sensor(keypoints=None): # amcap: reset all settings; autoexposure off; saturation max
    camera = CvPreprocVideoCamera(source=1,
                    crop=[320-128-10, 240-128+10, 320+128-10, 240+128+10], 
                    exposure=-7)
    for _ in range(5): camera.read() # Hack - camera transient
    return AsyncProcessor(CameraStreamProcessor(camera=camera,
            pipeline=[
                CvBlobDetector(
                    min_threshold=33,
                    max_threshold=199,
                    filter_by_color=True,
                    blob_color=255,
                    filter_by_area=True,
                    min_area=23.4,
                    max_area=2000,#119,
                    filter_by_circularity=True,
                    min_circularity=0.42,
                    filter_by_inertia=True,
                    min_inertia_ratio=0.29,
                    filter_by_convexity=True,
                    min_convexity=0.57),
                NearestNeighbourTracker(threshold=20, keypoints=keypoints),
                KeypointEncoder()],
            view=KeypointView(color=(0,255,0)),
            display=CvVideoDisplay(name="sensor"),
            writer=CvImageOutputFileSeq()))


with SyncRobot(Controller()) as robot:
    robot.linear_speed = 100
    robot.coord_frame = (300, 0, -100, 0, 0, 0)  
    robot.move_linear([0, 0, 0, 0, 0, 0])
    
    print("\nTest just the sensor")         
    with make_sensor() as sensor:       
        init_keypoints = sensor.process(num_frames=1, start_frame=1)[0,]
        print(f"keypoints.shape={init_keypoints.shape}")     

    with make_sensor(init_keypoints) as sensor:

        print("\nTest tap synchronous keypoints capture")
        robot.move_linear([35, 0, -20, 0, 0, 0])
        robot.move_linear([35, 0, -20-5, 0, 0, 0])
        keypoints = sensor.process(num_frames=5, start_frame=1) 
        robot.move_linear([35, 0, -20, 0, 0, 0])
        print(f"keypoints.shape={keypoints.shape}") 
            
        print("\nTest tap asynchronous keypoints capture")
        robot.move_linear([35, 0, -20, 0, 0, 0])
        sensor.async_process(num_frames=30, start_frame=1) 
        robot.move_linear([35, 0, -20-5, 0, 0, 0])
        robot.move_linear([35, 0, -20, 0, 0, 0])
        keypoints = sensor.async_result()
        print(f"keypoints.shape={keypoints.shape}") 

        robot.move_linear([0, 0, 0, 0, 0, 0])

# display results        
import matplotlib.pylab as plt 
plt.figure()
for i, kp_frame in enumerate(keypoints):
    plt.clf()
    plt.title("Keypoint frame "+str(i+1))
    plt.scatter(kp_frame[:, 0], kp_frame[:, 1], c='k')
    plt.axis("equal")
    plt.pause(0.1)
plt.show()
