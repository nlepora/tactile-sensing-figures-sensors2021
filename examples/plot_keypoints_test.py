# set up for 127-pin tip on mini-TacTip

import os, sys
from numpy import square, mean, sqrt
from cri.robot import SyncRobot
from cri.controller import Mg400Controller as Controller
from vsp.video_stream import CvImageOutputFileSeq, CvVideoDisplay, CvPreprocVideoCamera  
from vsp.processor import CameraStreamProcessor, AsyncProcessor
from vsp.detector import CvBlobDetector
from vsp.tracker import NearestNeighbourTracker
from vsp.encoder import KeypointEncoder
from vsp.view import KeypointView

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + r'\..')


def make_sensor(keypoints=None): # amcap: reset all settings; autoexposure off; saturdation max
    camera = CvPreprocVideoCamera(source=1,
                    crop=[320-128-30, 240-128-10, 320+128-30, 240+128-10],
                    exposure=-6)
    for _ in range(5): camera.read() # Hack - camera transient
    return AsyncProcessor(CameraStreamProcessor(camera=camera,
            pipeline=[
                CvBlobDetector(
                    min_threshold=33,
                    max_threshold=199,
                    filter_by_color=True,
                    blob_color=255,
                    filter_by_area=True,
                    min_area=13.4,#23.4
                    max_area=200,#119,
                    filter_by_circularity=True,
                    min_circularity=0.42,
                    filter_by_inertia=True,
                    min_inertia_ratio=0.29,
                    filter_by_convexity=True,
                    min_convexity=0.57),
                NearestNeighbourTracker(threshold=20, keypoints=keypoints),
                KeypointEncoder()],
            view=KeypointView(color=(0,255,0)),
            display=CvVideoDisplay(name="sensor"),
            writer=CvImageOutputFileSeq()))


with SyncRobot(Controller()) as robot:
    robot.linear_speed = 10
    robot.coord_frame = (310, 0, -113, 0, 0, 0)  
    robot.move_linear([0, 0, 0, 0, 0, 0])

    from common.visualisation.plot_keypoints import PlotKeypoints
    fig1 = PlotKeypoints("keypoints")        
    fig2 = PlotKeypoints("shears")        
    fig3 = PlotKeypoints("xshears")        

    with make_sensor() as sensor:       
        init_kps = sensor.process(num_frames=1, start_frame=1)[0,]
        print(f"init_keypoints.shape={init_kps.shape}") 

    with make_sensor(init_kps) as sensor:

        for i in range(1):
            robot.move_linear([0, 0, -20, 0, 0, 0])
            sensor.async_process(num_frames=50, start_frame=1) 
            robot.move_linear([0, 0, -20-5, 0, 0, 0])
            robot.move_linear([0, 0, -20, 0, 0, 0])
            
            kps = sensor.async_result()
            print(f"keypoints.shape={kps.shape}") 
            shears = sqrt(mean(square(kps - init_kps)[...,:2], axis=2))/10
            xshears = (kps - init_kps)[...,0]/10
            fig1.update(kps)
            fig2.update(kps, shears)
            fig3.update(kps, xshears)

    robot.move_linear([0, 0, 0, 0, 0, 0])
    
fig1.finish(r"C:\Temp")
fig2.finish(r"C:\Temp")
fig3.finish(r"C:\Temp")
