import os, sys
from cri.robot import SyncRobot
from cri.controller import Mg400Controller as Controller
from vsp.video_stream import CvImageOutputFileSeq, CvVideoDisplay, CvPreprocVideoCamera  
from vsp.processor import CameraStreamProcessor, AsyncProcessor

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + r'\..')


def make_sensor(): # amcap: reset all settings; autoexposure off; saturdation max
    camera = CvPreprocVideoCamera(source=1,
                crop=[320-128-10, 240-128+10, 320+128-10, 240+128+10],
                size=[256,256], 
                threshold=[61,-5],
                exposure=-6)
    for _ in range(5): camera.read() # Hack - camera transient    
    return AsyncProcessor(CameraStreamProcessor(camera=camera, 
                display=CvVideoDisplay(name='sensor'),
                writer=CvImageOutputFileSeq()))


with SyncRobot(Controller()) as robot:
    robot.linear_speed = 10
    robot.coord_frame = [310, 0, -113, 0, 0, 0]  
    robot.move_linear([0, 0, 0, 0, 0, 0])
        
    from common.visualisation.plot_frames import PlotFrames
    fig1 = PlotFrames("frames1")        
    fig2 = PlotFrames("frames2")    

    with make_sensor() as sensor: 

        for i in range(1):
            robot.move_linear([0, 0, -20, 0, 0, 0])

            sensor.async_process(num_frames=50, start_frame=1) 
            robot.move_linear([0, 0, -20-5, 0, 0, 0])
            robot.move_linear([0, 0, -20, 0, 0, 0])       
            frames = sensor.async_result()

            print(f'frames.shape={frames.shape}') 
            fig1.update(frames/255)
            fig2.update(1-frames/255)

    robot.move_linear([0, 0, 0, 0, 0, 0])

fig1.finish(r"C:\Temp")
fig2.finish(r"C:\Temp")
