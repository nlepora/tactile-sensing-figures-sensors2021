import os, sys
from cri.robot import SyncRobot
from cri.controller import Mg400Controller as Controller
from vsp.video_stream import CvImageOutputFileSeq, CvVideoDisplay, CvPreprocVideoCamera  
from vsp.processor import CameraStreamProcessor, AsyncProcessor
from vsp.detector import CvBlobDetector
from vsp.tracker import NearestNeighbourTracker
from vsp.encoder import KeypointEncoder
from vsp.view import KeypointView

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + r'\..')
from common.matlab.transformVoronoi import TransformVoronoi as Voronoi


def make_sensor(keypoints=None): # amcap: reset all settings; autoexposure off; saturdation max
    camera = CvPreprocVideoCamera(source=1, 
                    crop=[160-10,70+15,490-10,400+15], 
                    exposure=-8)
    for _ in range(5): camera.read() # Hack - camera transient
    return AsyncProcessor(CameraStreamProcessor(camera=camera,
            pipeline=[
                CvBlobDetector(
                    min_threshold=33,
                    max_threshold=199,
                    filter_by_color=True,
                    blob_color=255,
                    filter_by_area=True,
                    min_area=23.4,
                    max_area=119,
                    filter_by_circularity=True,
                    min_circularity=0.42,
                    filter_by_inertia=True,
                    min_inertia_ratio=0.29,
                    filter_by_convexity=True,
                    min_convexity=0.57),
                NearestNeighbourTracker(threshold=20, keypoints=keypoints),
                KeypointEncoder()],
            view=KeypointView(color=(0,255,0)),
            display=CvVideoDisplay(name='sensor'),
            writer=CvImageOutputFileSeq()))


print('\nTest just the sensor')         
with make_sensor() as sensor:       
    keypoints = sensor.process(num_frames=1, start_frame=1)[0,]
    areas, cx, cy = Voronoi().process(keypoints)
    print(f'voronoi.shape={areas.shape}')   


with SyncRobot(Controller()) as robot, make_sensor(keypoints) as sensor: 

    print('\nTest tap synchronous keypoints capture')
    robot.move_linear([250, 0, -15, 0, 0, 0])
    robot.move_linear([250, 0, -15-5, 0, 0, 0])
    keypoints = sensor.process(num_frames=5, start_frame=1) 
    areas, cx, cy = Voronoi().process(keypoints)
    robot.move_linear([250, 0, -15, 0, 0, 0])
    print(f'voronoi.shape={areas.shape}', end='') 
        
    print('\nTest tap asynchronous keypoints capture')
    robot.move_linear([250, 0, -15, 0, 0, 0])
    sensor.async_process(num_frames=20, start_frame=1) 
    robot.move_linear([250, 0, -15-5, 0, 0, 0])
    robot.move_linear([250, 0, -15, 0, 0, 0])
    keypoints = sensor.async_result()
    areas, cx, cy = Voronoi().process(keypoints)
    print(f'voronoi.shape={areas.shape}', end='') 

    robot.move_linear([200, 0, 50, 0, 0, 0])
