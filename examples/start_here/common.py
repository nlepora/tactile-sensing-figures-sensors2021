"""
Author: Elizabeth A. Stone (lizzie.stone@brl.ac.uk)

Use:
Compilation of commonly used functions in tactile experiments. These are
generic so as to be easily used in many different experiments. 
See minimum_example.py for an example of how these functions are used.
"""

import os
import json
import numpy as np

from taclab.core.sensor.preproc_video_camera import CvPreprocVideoCamera

from cri.robot import AsyncRobot, SyncRobot
from cri.controller import Mg400Controller as Controller

from vsp.video_stream import CvImageOutputFileSeq, CvVideoDisplay
from vsp.detector import CvBlobDetector
from vsp.tracker import NearestNeighbourTracker
from vsp.encoder import KeypointEncoder
from vsp.view import KeypointView
from vsp.processor import CameraStreamProcessor, AsyncProcessor


def make_robot():
    """ Create and return robot instance """
    return AsyncRobot(SyncRobot(Controller()))


def make_sensor_frames(meta):
    """
    Set up camera, setting the exposure, brightness etc and reading a few frames 
    until the settings are implemented (hacky implementation). Then create and
    return tactile sensor instance using settings from meta data.
    Current sensor is initialised to grab frames.
    """
    camera = CvPreprocVideoCamera(
        source=meta["source"],
        crop=meta["crop"],
        size=meta["size"], 
        threshold=meta["threshold"],
        exposure=meta["exposure"],
        brightness=meta["brightness"],
        contrast=meta["contrast"],
    ) 
    for _ in range(5):
        camera.read()  # Hack - camera transient
    return AsyncProcessor(
        CameraStreamProcessor(
            camera=camera, 
            display=CvVideoDisplay(name='sensor'),
            writer=CvImageOutputFileSeq()
        )
    )


def make_sensor_kps(meta, keypoints=None):
    """
    Set up camera, setting the exposure, brightness etc and clearing the buffer
    until the settings are implemented (hacky implementation). Then create and
    return tactile sensor instance using settings from meta data.
    Current sensor is initialised to use pin detection with nearest neighbour
    tracking.
    """
    camera = CvPreprocVideoCamera(
        source=meta["source"],
        crop=meta["crop"],
        exposure=meta["exposure"],
        brightness=meta["brightness"],
        contrast=meta["contrast"],
    ) 
    for _ in range(5):
        camera.read()  # Hack - camera transient
    return AsyncProcessor(
        CameraStreamProcessor(
            camera=camera,
            pipeline=[
                CvBlobDetector(
                    min_threshold=meta["min_threshold"],
                    max_threshold=meta["max_threshold"],
                    filter_by_color=meta["filter_by_color"],
                    blob_color=meta["blob_color"],
                    filter_by_area=meta["filter_by_area"],
                    min_area=meta["min_area"],
                    max_area=meta["max_area"],
                    filter_by_circularity=meta["filter_by_circularity"],
                    min_circularity=meta["min_circularity"],
                    filter_by_inertia=meta["filter_by_inertia"],
                    min_inertia_ratio=meta["min_inertia_ratio"],
                    filter_by_convexity=meta["filter_by_convexity"],
                    min_convexity=meta["min_convexity"],
                ),
                NearestNeighbourTracker(
                    threshold=meta["nntracker_threshold"], keypoints=keypoints
                ),
                KeypointEncoder(),
            ],
            view=KeypointView(color=meta["kpview_colour"]),
            display=CvVideoDisplay(name="sensor"),
            writer=CvImageOutputFileSeq(),
        )
    )


def save_keypoint_data(data, meta, name=None):
    """
    Save data to a json file (named data.json) in the same directory as
    meta data file was saved. Data can be a list, np.array, dictionary (note, when adding
    np.arrays to dictionary use <array_name>.tolist() as json.dump cannot handle
    np.arrays), or anything that can be handled by json.dump
    """

    print("type keypoints", type(data))

    data = np.around(data, 2) #  limit precision to rounded
    data = data[:,:,0:2] # remove weird third column of data

    print("\n data.shape", data.shape) 

    if type(data) is list:
        # horrible hack to make sure all sub lists that may be np.arrays get converted to lists
        data = np.array(data)
        data = data.tolist()

    elif type(data) is np.ndarray:
        # make sure np.array becomes list
        data = data.tolist()

    # remove meta.json bit to add new name
    part_path, _ = os.path.split(meta["meta_file"])

    if name is not None:
        with open(os.path.join(meta["home_dir"], part_path, name), "w") as f:
            json.dump(data, f)
    else:
        with open(os.path.join(meta["home_dir"], part_path, "data.json"), "w") as f:
            json.dump(data, f)


def init_robot(robot, meta, do_homing=False):
    """
    Set tcp, speeds and frames using the parameters from meta data, then move
    arm to home location, then switch the robot back to the work frame.
    """

    # Set TCP, linear speed,  angular speed and coordinate frame
    robot.tcp = meta["robot_tcp"]
    robot.linear_speed = meta["linear_speed"]
    robot.angular_speed = meta["angular_speed"]

    if do_homing:
        # calibrate arm
        go_home(robot, meta)  # so not out wide and stupid
        robot.sync_robot.perform_homing()

    go_home(robot, meta)


def go_home(robot, meta):
    """ Go to safe home position """

    robot.coord_frame = meta["base_frame"]
    robot.move_linear(meta["home_pose"])
    # Return to work frame
    robot.coord_frame = meta["work_frame"]


def tap_at(location, orientation, robot, sensor, meta):
    """
    Move to location and tap with the given orientation, returning keypoints

    :param orientation: must be in DEGREES (not radians)
    """
    full_cartesian_location = np.array([location[0], location[1], 0, 0, 0, orientation])

    # make sure in work frame - should already be set, but just to make sure
    robot.coord_frame = meta["work_frame"]

    # move to given position
    robot.move_linear(full_cartesian_location)

    # change coordinate frame so origin is the new position
    robot.coord_frame = meta["base_frame"]
    robot.coord_frame = robot.pose

    # name to save figure with path 
    part_path, _ = os.path.split(meta["meta_file"])
    name = os.path.join(meta["home_dir"], part_path, "frames.png")

    # tap relative to new origin, using 2 part tap movement specified in meta
    robot.move_linear(meta["tap_move"][0])
    data = sensor.process(meta["num_frames"], start_frame=1, outfile=name)
    robot.move_linear(meta["tap_move"][1])

    # return to work frame
    robot.coord_frame = meta["work_frame"]

    return data 
