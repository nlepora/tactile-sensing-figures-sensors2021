"""
Author: Elizabeth A. Stone (lizzie.stone@brl.ac.uk)

Use:
A minimum working example of the taclab code running with a dobot robot arm.

This program sets up the robot arm and tactile sensor, saving meta data about
the current settings, and then does a single tap on an object (assuming there
is an object just below the location specified by work_frame).  The tap data is
keypoints (pin locations) and is saved to a data file and shown on a scatter
graph. The robot then returns to the home location, and the program closes.

The main variables for this program are stored in meta, as configured in
make_meta(), and these may need tweaking for your specific setup.
"""

import numpy as np
import os
import time
import json

import taclab.start_here.common as common

np.set_printoptions(precision=2, suppress=True)


def make_meta():
    """
    Make dictionary of all meta data about the current experiment, and
    save to json file.
    """

    # name for this experiment run folder and meta file (will be added to end
    # of "home_dir" for full path name)
    data_path = os.path.join("min_example",
        os.path.basename(__file__)[:-3] + "_" + time.strftime("%y%m%d_%H%M")         )

    meta = {
        # ~~~~~~~~~ Paths ~~~~~~~~~#
        "home_dir": os.path.join(r"C:\Temp", "TacTip_dobot"),
        "meta_file": os.path.join(data_path, "meta.json"),
        # "image_dir": None,
        # "image_df_file": None,
        # "ip": None,
        # ~~~~~~~~~ Robot movements ~~~~~~~~~#
        "robot_tcp": [0, 0, 0, 0, 0, 0],  # tool center point in mm - change for different sensors
        "base_frame": [0, 0, 0, 0, 0, 0],  # see dobot manual for location
        "home_pose": [300, 0, 0, 0, 0, 0],  # choose a safe "resting" pose (in base frame)
        "work_frame": [340, 0, -133, 0, 0, 0],  # experiment specific start point (in base frame)
        "linear_speed": 10,
        "angular_speed": 100,
        "tap_move": [[0, 0, -5, 0, 0, 0], [0, 0, 0, 0, 0, 0]],
        # "poses_rng": None,
        # "obj_poses": None,
        # "num_poses": None,
        # ~~~~~~~~~ CV Settings ~~~~~~~~~#
        "min_threshold": 33,
        "max_threshold": 199,
        "filter_by_color": True,
        "blob_color": 255,
        "filter_by_area": True,
        "min_area": 23.4,
        "max_area": 2000,#119,
        "filter_by_circularity": True,
        "min_circularity": 0.42,
        "filter_by_inertia": True,
        "min_inertia_ratio": 0.29,
        "filter_by_convexity": True,
        "min_convexity": 0.57,
        "nntracker_threshold": 20,
        "kpview_colour": (0, 255, 0),
        # ~~~~~~~~~ Camera Settings ~~~~~~~~~#
        "exposure": -7,  
        "crop": [320-128, 240-128, 320+128, 240+128],  # usually needs tuning for camera (values for miniTip)
        "brightness": 255,    # does not work windows 10
        "contrast": 255,      # does not work windows 10
        "source": 1,          # 0, 1 or 2 - common cause of errors
        # ~~~~~~~~~ Processing Settings ~~~~~~~~~#
        "num_frames": 10,
        # ~~~~~~~~~ Run specific comments ~~~~~~~~~#
        "comments": "just testing",  # something so you can identify runs later, if you wish
        }

    os.makedirs(os.path.join(meta["home_dir"], os.path.dirname(meta["meta_file"])))
    with open(os.path.join(meta["home_dir"], meta["meta_file"]), "w") as f:
        json.dump(meta, f)

    return meta


def main():
    # make the dictionary of metadata about current experiment and save it to
    # a file ("meta.json"). "meta" stores important variables specific to the current
    # experiment, allowing a record of what settings were used in any experiment
    # run. Variables in meta should be set manually and not changed through code.
    # Important (i.e. most) variables should be set in meta so they can be saved
    # to file and referenced in code - the main exception is data collected throughout
    # an experiment, which should be saved elsewhere (i.e. using common.save_data() ).
    # NOTE, it is important to check the variables in "make_meta()" are correct for
    # your setup (sensor settings, robot frames, "home_dir" paths etc)
    meta = make_meta()

    # "with" statement is important so that the __exit__() function is called on
    # leaving the "with" block. In this case, leaving the "with" block causes
    # the robot and sensor to disconnect (this happens even when an error causes
    # early termination of code)
    with common.make_robot() as robot, common.make_sensor_kps(meta) as sensor:

        # set velocities and frames of robot, and make it go to safe home position
        # as defined in meta. Optionally, can perform homing which calibrates
        # robot joint rotations
        common.init_robot(robot, meta, do_homing=False)

        print("Main code...")

        # do a tap at [x,y] = [0,0], relative to work frame, with a sensor
        # rotation of 0 using the motion defined in meta (as "tap_move"),
        # returning pin locations as "keypoints"
        keypoints = common.tap_at([0, 0], 0, robot, sensor, meta)

        # save the data to file (default file name is "data.json")
        common.save_keypoint_data(keypoints, meta)

        # show plot of tap taken (note, this pauses program until graph is closed)
        import matplotlib.pyplot as plt   # can cause magician dll import error if in header
        plt.axis("equal")
        for kp_frame in keypoints:
            plt.clf()
            plt.scatter(kp_frame[:, 0], kp_frame[:, 1])
            plt.pause(0.1)
        plt.show()

        # return to safe home position
        common.go_home(robot, meta)

    print("Done, exiting")


if __name__ == "__main__":
    main()
